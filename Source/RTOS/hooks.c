#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"


/** 
@brief Function called from the idle task.
This hook can be used to execute a background task.
Do not use blocking calls.
configUSE_IDLE_HOOK must be set to 1 in FreeRTOSConfig.h

*/
void vApplicationIdleHook (void)
{
    __WFI();
}

/** 
@brief Function called upon each tick. Runs from ISR context.
configUSE_TICK_HOOK must be set to 1 in FreeRTOSConfig.h
*/
void vApplicationTickHook (void)
{
    
}

/** 
@brief Function called when an attempt to allocate dynamic memory fails.
Not used in case dynamic memory allocation is not supported.
configUSE_MALLOC_FAILED_HOOK must be set to 1 in FreeRTOSConfig.h
*/
void vApplicationMallocFailedHook (void)
{
    while(1);
}

/** 
@brief Function called once when the daemon tasks starts running.
Can be used to run initialization functions that make use of FreeRTOS functionality.
configUSE_DAEMON_TASK_STARTUP_HOOK must be set to 1 in FreeRTOSConfig.h
*/
void vApplicationDaemonTaskStartupHook (void)
{
    
}

/** 
@brief Function called in case a task stack overflow is detected.
configCHECK_FOR_STACK_OVERFLOW must be set to 1 or 2 in FreeRTOSConfig.h
*/
void vApplicationStackOverflowHook (TaskHandle_t xTask, signed char *pcTaskName )
{
    while(1);
}


/** 
@brief Function that provides the stack for the idle task.
This function is required by FreeRTOS in case static memory allocation is used.
*/
void vApplicationGetIdleTaskMemory (StaticTask_t **ppxIdleTaskTCBBuffer,
                                    StackType_t **ppxIdleTaskStackBuffer,
                                    uint32_t *pulIdleTaskStackSize)
{
    static StaticTask_t xIdleTaskTCB;
    static StackType_t uxIdleTaskStack[configIDLE_TASK_STACK_SIZE];

    *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;
    *ppxIdleTaskStackBuffer = uxIdleTaskStack;
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}

/** 
@brief Function that provides the stack for the idle task.
This function is required by FreeRTOS in case static memory allocation is used.
*/
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
                                     StackType_t **ppxTimerTaskStackBuffer,
                                     uint32_t *pulTimerTaskStackSize )
{
    static StaticTask_t xTimerTaskTCB;
    static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

    *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;
    *ppxTimerTaskStackBuffer = uxTimerTaskStack;
    *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}