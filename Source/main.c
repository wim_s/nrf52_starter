#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "nrf.h"

#define DEMO_TASK_PRIORITY       1
#define DEMO_TASK_STACK_SIZE     400


static StackType_t demo_task_stack[DEMO_TASK_STACK_SIZE];
static TaskHandle_t demo_task_handle;
static StaticTask_t demo_task_tcb;

static void DemoTask (void* param)
{
    // blink LED 1 (P0.17)
    NRF_GPIO->PIN_CNF[17] = 1;
    
    while (1)
    {
        NRF_GPIO->OUTSET = (1 << 17);
        vTaskDelay(pdMS_TO_TICKS(1000));
        NRF_GPIO->OUTCLR = (1 << 17);
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}


void main(void) 
{
    demo_task_handle = xTaskCreateStatic(DemoTask, "DEMO", DEMO_TASK_STACK_SIZE, NULL, DEMO_TASK_PRIORITY, demo_task_stack, &demo_task_tcb);

    vTaskStartScheduler();

    while (1);
}


